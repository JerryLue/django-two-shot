# import a forms template from django
from django import forms


# define a login form using the built in forms model
class LoginForm(forms.Form):
    # defines a username field with max length 150
    username = forms.CharField(max_length=150)
    # defines a password field with max length 150
    password = forms.CharField(
        max_length=150,
        # defines this field as a password field using a widget
        # that determines if the value will be shown when the form is
        # redisplayed after a validation error automatically set to false
        # for safety passwords should be obscured and hashed when stored
        widget=forms.PasswordInput,
    )


# define a Signup form with username, password, and password_confirmation
class SignupForm(forms.Form):
    # defines a username field with max length 150
    username = forms.CharField(max_length=150)
    # defines a password field with max length 150
    password = forms.CharField(
        max_length=150,
        # defines this field as a password field using a widget
        # that determines if the value will be shown when the form is
        # redisplayed after a validation error automatically set to false
        # for safety passwords should be obscured and hashed when stored
        widget=forms.PasswordInput,
    )
    # creates a second password field identical to the first to
    # confirm the password and should only pass if the fields match
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
