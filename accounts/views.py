# redirect returns an HttpResponseRedirect to the appropriate URL
# for the arguments passed
# render combines a template with a context dictionary and
# returns an HttpResponse object with rendered text
from django.shortcuts import render, redirect

# login saves the user's ID in the session
# any data set during the anonymous session is retained in the
# session after a user logs in
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required

# user model is essentially a profile with editable information fields
from django.contrib.auth.models import User

# imports the sign up form you created in forms.py
from accounts.forms import LoginForm, SignupForm
from receipts.models import Account

# Create your views here.


# defines a login function that occurs when someone submits login credentials
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            # sets username to the input from the website login page username field
            username = form.cleaned_data["username"]
            # sets password to the input from the website login page password field
            password = form.cleaned_data["password"]
            # sets a variable user by passing in the above fields
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            # checks if user credentials match an account in our database
            if user is not None:
                # if there is a match log them in
                login(request, user)
                # redirect them to sucess page
                return redirect("home")
    # otherwise
    else:
        # if request method is GET, display a blank login form
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)
    # return invalid login error message
    ##user.add_error("login","Login credentials do not match our system")


# define a logout function that returns the user to the login page
def user_logout(request):
    logout(request)
    return redirect("login")


# define a signup function so people can make an account
def signup(request):
    # POST is when the person has submitted the form or clicks enter
    # if the request method is POST
    if request.method == "POST":
        # set variable form equal to the sign up form in forms.py
        # passing in the user input as request.POST
        form = SignupForm(request.POST)
        # checks if the form is valid and follows the
        # limitations set in the forms.py
        if form.is_valid():
            # sets the form username to the cleaned username input
            # cleaned_data returns a validated form input field and value
            # where strings are returned as objects.
            username = form.cleaned_data["username"]
            # sets the form password to the cleaned password input
            password = form.cleaned_data["password"]
            # sets the form password_confirmation to the cleaned password_confirmation input
            password_confirmation = form.cleaned_data["password_confirmation"]
            # checks if password is equal to password_confirmation
            if password == password_confirmation:
                # creates a user by passing in the cleaned inputs provided
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # saves the user info
                user.save()
                # logs them in using info
                login(request, user)
                # redirects them to the home page
                return redirect("home")
            # if passwords dont match
            else:
                # generates a form error
                form.add_error("password", "the passwords do no match")
    # if the request was not a post method and a get method instead
    else:
        # generate a blank form
        form = SignupForm()
    # pass in user input data form as a context dictionary
    # we can use "form" to call this data later
    context = {
        "form": form,
    }
    # renders a version of the context data in the signup html
    return render(request, "accounts/signup.html", context)
