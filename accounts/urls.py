# path is native to django and allows you to establish
# a url pattern. The first part is an argument where you establish the
# input for the path, the second is the function that you created for
# redirecting the path, the last is the name assigned to the path so you can
# import and call it in other python modules
from django.urls import path

# this imports functions from your views.py to pass through the urlpatterns
from accounts.views import user_login, user_logout, signup


# url patterns is a list of paths to help redirect the user when input
# is provided
# path(location/html path, views function that was imported, name=usually same name as views function)
urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
