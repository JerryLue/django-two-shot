# import a built in template from django that is for creating forms
from django.forms import ModelForm

# call the recipe model you made for entering fields into the django recipe app
from receipts.models import Receipt, ExpenseCategory, Account


# create a class called RecipeForm to
class ReceiptForm(ModelForm):
    # meta is a subclass within a class
    class Meta:
        # use the recipe form made in the models.py and use those fields in the template
        # only add the fields that people should be able to create so not datetime etc.
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


# define an expensecategory form to assist in creating a new expense category
class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


# define an Account form to assist in creating a new account
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
