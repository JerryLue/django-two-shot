from django.db import models

# setting allows you to access the User model
from django.conf import settings

# Create your models here.
# Feature 3 create models for ExpenseCategory, Receipt, Account


# define a model for ExpenseCategory with name, owner, cascade on delete
# this class is a value that the user can apply to receipts like "gas" "entertainment"
class ExpenseCategory(models.Model):
    # name field with max length 50 characters
    name = models.CharField(max_length=50)
    # owner field refers to djangos user model
    # foreign key access to data from another model
    owner = models.ForeignKey(
        # this is the recommended way to access the user model
        settings.AUTH_USER_MODEL,
        # set a related name of categories
        # this is used in the HTML to call the model
        related_name="categories",
        # when the user is deleted so is the ExpenseCategory model
        on_delete=models.CASCADE,
    )

    # returns expensecategory objects as the name they are assigned
    # instead of the default object number
    def __str__(self):
        return self.name


# define an account model with a name, number, and owner
# this class is the way that the user will pay expenses
class Account(models.Model):
    # name field with max length 100 characters
    name = models.CharField(max_length=100)
    # number field as a string with max 20 characters
    ##
    ##DO WE NEED TO CONVERT STR TO INT AT SOME POINT? - refer to feature 3
    ##
    number = models.CharField(max_length=20)
    # owner field refers to djangos user model
    # foreign key access to data from another model
    owner = models.ForeignKey(
        # this is the recommended way to access the user model
        settings.AUTH_USER_MODEL,
        # set a related name of accounts
        # this is used in the HTML to call the model
        related_name="accounts",
        # when the user is deleted so is the Account model
        on_delete=models.CASCADE,
    )

    # returns account objects as the name they are assigned
    # instead of the default object number
    def __str__(self):
        return self.name


# define a receipt model with a vendor, total, tax, date, purchaser, category,
# and account
# receipt model is the primary thing that his application keeps track of for accounting purposes
# THIS MODEL USES DECIMAL FIELD - refer to feature 3
class Receipt(models.Model):
    # creates a vendor field as a string with max 200 characters
    vendor = models.CharField(max_length=200)
    # creates a total field as a float with 3 decimal places and 10 max digits
    total = models.DecimalField(decimal_places=3, max_digits=10)
    # creates a tax field as a float with 3 decimal places and 10 max digits
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    # creates a date field as an automatic DateTime field
    date = models.DateTimeField(auto_created=True)
    # purchaser field refers to djangos user model
    # foreign key access to data from another model
    purchaser = models.ForeignKey(
        # this is the recommended way to access the user model
        settings.AUTH_USER_MODEL,
        # set a related name of receipts
        # this is used in the HTML to call the model
        related_name="receipts",
        # when the user is deleted so is the Receipt model
        on_delete=models.CASCADE,
    )
    # category field refers to ExpenseCategory model you created above
    # foreign key access to data from another model
    category = models.ForeignKey(
        # calls the ExpenseCategoty model you made above
        ExpenseCategory,
        # set a related name of receipts
        # this is used in the HTML to call the model
        related_name="receipts",
        # when the ExpenseCategory is deleted so is the category model
        on_delete=models.CASCADE,
    )
    # account field refers to Account model you created above
    # foreign key access to data from another model
    account = models.ForeignKey(
        # calls the Account model you made above
        Account,
        # set a related name of receipts
        # this is used in the HTML to call the model
        related_name="receipts",
        # when the Account is deleted so is the receipt account field
        on_delete=models.CASCADE,
        # for existing accounts without an receipt account field fill it with null
        null=True,
    )

    # returns receipt objects as the vendor they are assigned
    # instead of the default object number
    def __str__(self):
        return self.vendor
