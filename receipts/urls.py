# path is native to django and allows you to establish
# a url pattern. The first part is an argument where you establish the
# input for the path, the second is the function that you created for
# redirecting the path, the last is the name assigned to the path so you can
# import and call it in other python modules
from django.urls import path

# this imports functions from your views.py to pass through the urlpatterns
from receipts.views import (
    receipts_list,
    create_receipt,
    account_list,
    category_list,
    create_category,
    create_account,
)


# url patterns is a list of paths to help redirect the user when input
# is provided
# path(location/html path, views function that was imported, name=usually same name as views function)
urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
# MAKE SURE SYNTAX IS CORRECT NAME = "" DO NOT PASS IN STRING FOR 3RD ARGUMENT
