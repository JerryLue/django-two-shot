from django.contrib import admin

# import your models from receipts models py here
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
# this allows you to access the models in your localhost/admin with your superuser


# register the ExpenseCategory model
@admin.register(ExpenseCategory)
# create a class passing ExpenseCategory model through the admin model
class ExpenseCategoryAdmin(admin.ModelAdmin):
    # list display is what columns you want created in admin
    list_display = (
        # adds name, owner, receipts, and id fields into the admin
        "name",
        "owner",
        "receipts",
        "id",
    )


# register the Account model
@admin.register(Account)
# create a class passing Account model through the admin model
class AccountAdmin(admin.ModelAdmin):
    # list display is what columns you want created in admin
    list_display = (
        # adds name, number, owner, receipts, and id fields into the admin
        "name",
        "number",
        "owner",
        "receipts",
        "id",
    )


# register the Receipt model
@admin.register(Receipt)
# create a class passing Receipt model through the admin model
class ReceiptAdmin(admin.ModelAdmin):
    # list display is what columns you want created in admin
    # only add immutable objects so something like date which does not need to be
    # changed should be excluded
    list_display = (
        # adds vendor, total, tax, purchaser, category, account, receipts, and id fields into the admin
        "vendor",
        "total",
        "tax",
        "purchaser",
        "category",
        "account",
        "id",
    )
