from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# import
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.form import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
# users must be logged in to view receipts list
@login_required
# define a view to see your receipts list
def receipts_list(request):
    # sets list equal to all the objects in the Receipt model
    lists = Receipt.objects.filter(purchaser=request.user)
    # creates context dictionary assigning these objects to "home"
    context = {
        "lists": lists,
    }
    # returns a rendered version with the context data in the list.html
    return render(request, "receipts/list.html", context)


# users must be logged in to view receipts list
@login_required
# define a function that will allow someone to create a receipt from the
# home page
def create_receipt(request):
    # check if the request is post or get
    # POST is when the person has submitted the form or clicks enter
    if request.method == "POST":
        # invokes models.py to call on fields in the receipt module
        form = ReceiptForm(request.POST)
        # use the model to validate the values in variable form
        if form.is_valid():
            # stores the form without saving it into a receipt variable
            receipt = form.save(False)
            # now set the receipt purchaser to the user who initiated the request
            receipt.purchaser = request.user
            # save them to the database
            receipt.save()
            form.save()
            # if all goes well, we can redirect the browser
            # to the home page and leave the function
            return redirect("home")
    else:
        # create and instance of the Django model form class
        form = ReceiptForm()
    # put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "receipts/create.html", context)


# define a view to see your receipts list
@login_required
def account_list(request):
    # sets list equal to all the objects in the Receipt model
    lists = Account.objects.filter(owner=request.user)
    # creates context dictionary assigning these objects to "home"
    context = {
        "lists": lists,
    }
    # returns a rendered version with the context data in the list.html
    return render(request, "receipts/accountslist.html", context)


# define a view to see your expense categories list
@login_required
def category_list(request):
    category_lists = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "lists": category_lists,
    }
    return render(request, "receipts/expenselist.html", context)


# define a create view to make new expense categories
@login_required
def create_category(request):
    # if the request method is post
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        # take the info and store it in a form
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    # context is set to form
    return render(request, "receipts/categories/create.html", context)


# define a create view to make new Accounts
@login_required
def create_account(request):
    # if request method is post
    if request.method == "POST":
        # set a form equal to the module form passing in the request
        form = AccountForm(request.POST)
        # if form is valid
        if form.is_valid():
            # store the form as a variable with save set to false
            new_account = form.save(False)
            # set the account to current user
            new_account.owner = request.user
            # save form
            form.save()
            # save variable
            new_account.save()
            # redirect to account view
            return redirect("account_list")
    # else
    else:
        # set form to the blank model form
        form = AccountForm()
    # context = form:form
    context = {"form": form}
    return render(request, "receipts/accounts/create.html", context)
